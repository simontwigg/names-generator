package com.inseinc.namegenerator.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

@Component
public class NameUtility {

	private String names = "names.properties";

	@Autowired
	private ResourceLoader resourceLoader;

	public List<String> getNames() throws Exception {
		Resource resource = resourceLoader.getResource("classpath:" + names);

		try (BufferedReader buff = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
			List<String> names = new ArrayList<String>();
			String name = null;

			while ((name = buff.readLine()) != null) {
				names.add(name);
			}

			return names;
		}
	}

	public ResourceLoader getResourceLoader() {
		return resourceLoader;
	}

	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

}
