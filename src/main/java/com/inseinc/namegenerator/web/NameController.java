package com.inseinc.namegenerator.web;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inseinc.namegenerator.util.NameUtility;

@RestController
public class NameController {

	@Autowired
	private NameUtility nameUtility;

	@GetMapping("/getName")
	public String getName() {
		String returnVal = null;
		Random rng = new Random();

		try {
			List<String> names = nameUtility.getNames();
			returnVal = names.get(rng.nextInt(names.size()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnVal;
	}

	public NameUtility getNameUtility() {
		return nameUtility;
	}

	public void setNameUtility(NameUtility nameUtility) {
		this.nameUtility = nameUtility;
	}

}
