FROM openjdk:8-alpine
EXPOSE 8080
VOLUME /tmp
ADD /target/*.jar name-generator-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/name-generator-0.0.1-SNAPSHOT.jar"]